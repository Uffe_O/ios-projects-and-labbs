//
//  ViewController.m
//  Labb1IOS
//
//  Created by ITHS on 2016-01-28.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (IBAction)changeColor:(UIButton *)sender {
    self.view.backgroundColor = [UIColor whiteColor];
}


@end
